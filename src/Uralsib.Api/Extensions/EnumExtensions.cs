﻿using System;
using Uralsib.Api.Models.Enums;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace Uralsib.Api.Extensions
{
    public static class EnumExtensions
    {
        public static UralsibEducation? ToUralsibEducation(this Education? education)
        {
            if (!education.HasValue)
                return null;
            switch (education.Value)
            {
                case Education.PrimarySchool:
                    return UralsibEducation.PrimarySchool;
                case Education.HighSchool:
                    return UralsibEducation.HighSchool;
                case Education.SpecializedHighSchool:
                    return UralsibEducation.SpecializedHighSchool;
                case Education.IncompleteSecondaryEducation:
                    return UralsibEducation.HighSchool;
                case Education.IncompleteHigherEducation:
                    return UralsibEducation.IncompleteHigherEducation;
                case Education.HigherEducation:
                    return UralsibEducation.HigherEducation;
                case Education.TwoOrMoreHigherEducations:
                    return UralsibEducation.TwoOrMoreHigherEducations;
                case Education.AcademicDegree:
                    return UralsibEducation.AcademicDegree;
                default:
                    throw new ArgumentException($"Invalid {nameof(education)} value", nameof(education));
            }
        }
        
        public static UralsibMaritalStatus? ToUralsibMaritalStatus(this MaritalStatus? maritalStatus)
        {
            if (!maritalStatus.HasValue)
                return null;
            switch (maritalStatus.Value)
            {
                case MaritalStatus.Single:
                    return UralsibMaritalStatus.Single;
                case MaritalStatus.Married:
                    return UralsibMaritalStatus.Married;
                case MaritalStatus.Widowed:
                    return UralsibMaritalStatus.Widowed;
                case MaritalStatus.Divorced:
                    return UralsibMaritalStatus.Divorced;
                case MaritalStatus.Remarriage:
                    return UralsibMaritalStatus.Remarriage;
                case MaritalStatus.CivilMarriage:
                    return UralsibMaritalStatus.CivilMarriage;
                default:
                    throw new ArgumentException($"Invalid {nameof(maritalStatus)} value", nameof(maritalStatus));
            }
        }
        
        public static string ToUralsibActivity(this Activity? activity, DateTime? dateOfBirth)
        {
            if (!activity.HasValue)
                return null;
            switch (activity.Value)
            {
                case Activity.Employee:
                    return UralsibActivity.Employee.GetDescription();
                case Activity.SelfEmployed:
                    return UralsibActivity.Business.GetDescription();
                case Activity.Unemployed:
                    return dateOfBirth.HasValue && (DateTime.Now - dateOfBirth.Value).TotalDays / 365.25 >= 60
                        ? UralsibActivity.Pensioner.GetDescription()
                        : UralsibActivity.Unknown.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(activity)} value", nameof(activity));
            }
        }

        public static string ToUralsibEmployerIndustry(this EmployerIndustry? employerIndustry)
        {
            if (!employerIndustry.HasValue)
                return null;
            switch (employerIndustry)
            {
                case EmployerIndustry.Banks:
                    return UralsibEmployerIndustry.Banks.GetDescription();
                case EmployerIndustry.CharityAndReligiousOrganizations:
                case EmployerIndustry.Military:
                case EmployerIndustry.Gambling:
                case EmployerIndustry.Mining:
                case EmployerIndustry.HumanResources:
                case EmployerIndustry.Manufacturing:
                case EmployerIndustry.Marketing:
                case EmployerIndustry.Insurance:
                case EmployerIndustry.Tourism:
                case EmployerIndustry.LegalServices:
                case EmployerIndustry.Other:
                    return UralsibEmployerIndustry.Other.GetDescription();
                case EmployerIndustry.CivilService:
                    return UralsibEmployerIndustry.CivilService.GetDescription();
                case EmployerIndustry.Healthcare:
                    return UralsibEmployerIndustry.Healthcare.GetDescription();
                case EmployerIndustry.InformationTechnology:
                    return UralsibEmployerIndustry.InformationTechnology.GetDescription();
                case EmployerIndustry.ScienceEducation:
                    return UralsibEmployerIndustry.ScienceEducation.GetDescription();
                case EmployerIndustry.Restaurants:
                    return UralsibEmployerIndustry.Restaurants.GetDescription();
                case EmployerIndustry.Agriculture:
                    return UralsibEmployerIndustry.Agriculture.GetDescription();
                case EmployerIndustry.Development:
                    return UralsibEmployerIndustry.Development.GetDescription();
                case EmployerIndustry.Trading:
                    return UralsibEmployerIndustry.Trading.GetDescription();
                case EmployerIndustry.Transport:
                    return UralsibEmployerIndustry.Transport.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(employerIndustry)} value", nameof(employerIndustry));
            }
        }

        public static string ToUralsibEmployeePosition(this EmployeePosition? employeePosition)
        {
            if (!employeePosition.HasValue)
                return null;
            switch (employeePosition)
            {
                case EmployeePosition.Employee:
                case EmployeePosition.Specialist:
                    return UralsibEmployeePosition.Specialist.GetDescription();
                case EmployeePosition.Manager:
                    return UralsibEmployeePosition.Manager.GetDescription();
                case EmployeePosition.Head:
                    return UralsibEmployeePosition.Head.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(employeePosition)} value", nameof(employeePosition));
            }
        }
    }
}