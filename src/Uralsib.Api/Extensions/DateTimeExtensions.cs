﻿using System;

namespace Uralsib.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToUralsibDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }   
        
        public static string ToUralsibDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : string.Empty;
        }
        
        public static string ToUralsibAppIdFormat(this DateTime date)
        {
            return date.ToString("yyyyMMdd");
        }
    }
}