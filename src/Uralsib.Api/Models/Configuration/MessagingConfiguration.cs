﻿namespace Uralsib.Api.Models.Configuration
{
    public class MessagingConfiguration
    {
        public int ChannelsCount { get; set; }
        
        public int MaxRetryCount { get; set; }
    }
}