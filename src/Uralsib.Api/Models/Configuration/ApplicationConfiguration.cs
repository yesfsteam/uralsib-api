﻿using System;

namespace Uralsib.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public string Source { get; set; }
        public string Prefix { get; set; }
        public string Product { get; set; }
        public Guid CreditOrganizationId { get; set; }
        public int MinCreditAmount { get; set; }
    }
}