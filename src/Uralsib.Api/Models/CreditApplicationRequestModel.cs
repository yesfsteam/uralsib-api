﻿using System;
using System.Net;
using Yes.Infrastructure.Common.Models;

namespace Uralsib.Api.Models
{
    public class CreditApplicationRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Идентификатор заявки Uralsib
        /// </summary>
        public string UralsibAppId { get; set; }
        
        /// <summary>
        /// Флаг одобрения Uralsib
        /// </summary>
        public int? UralsibApproveFlag { get; set; }
        
        /// <summary>
        /// Сообщение для клиента Uralsib
        /// </summary>
        public string UralsibMessage { get; set; }
        
        /// <summary>
        /// Код ошибки Uralsib
        /// </summary>
        public string UralsibErrorCode { get; set; }
        
        /// <summary>
        /// Текст ошибки Uralsib
        /// </summary>
        public string UralsibErrorMessage { get; set; }
    }
}