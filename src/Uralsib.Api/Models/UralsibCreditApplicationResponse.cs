﻿using Yes.Infrastructure.Common.Models;

namespace Uralsib.Api.Models
{
    public class UralsibCreditApplicationResponse : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки
        /// </summary>
        public string AppId { get; set; }
        
        /// <summary>
        /// Флаг одобрения
        /// </summary>
        public int ApproveFlag { get; set; }
        
        /// <summary>
        /// Сообщение для клиента
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// Код ошибки
        /// </summary>
        public string ErrorCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}