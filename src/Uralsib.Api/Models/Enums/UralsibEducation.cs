﻿﻿ namespace Uralsib.Api.Models.Enums
{
    public enum UralsibEducation
    {
        PrimarySchool = 0,
        HighSchool = 1,
        SpecializedHighSchool = 2,
        IncompleteHigherEducation = 3,
        HigherEducation = 4,
        TwoOrMoreHigherEducations = 5,
        AcademicDegree = 6
    }
}