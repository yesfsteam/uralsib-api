﻿namespace Uralsib.Api.Models.Enums
{
    public enum UralsibMaritalStatus
    {
        Single = 1,
        Married = 2,
        Widowed = 3,
        Divorced = 4,
        Remarriage = 5,
        //Other = 5,
        CivilMarriage = 7
    }
}