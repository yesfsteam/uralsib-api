﻿﻿using System.ComponentModel;

 namespace Uralsib.Api.Models.Enums
{
    public enum UralsibEmployerIndustry
    {
        [Description("FI")]
        Banks = 1,
        [Description("OT")]
        Other = 2,
        [Description("PU")]
        CivilService = 3,
        [Description("MD")]
        Healthcare = 4,
        [Description("IT")]
        InformationTechnology = 5,
        [Description("ES")]
        ScienceEducation = 6,
        [Description("HO")]
        Restaurants = 7,
        [Description("AG")]
        Agriculture = 8,
        [Description("CO")]
        Development = 9,
        [Description("CR")]
        Trading = 10,
        [Description("TR")]
        Transport = 11
    }
}