﻿﻿using System.ComponentModel;

 namespace Uralsib.Api.Models.Enums
{
    public enum UralsibActivity
    {
        [Description("06")]
        Employee = 1,
        [Description("07")]
        Business = 2,
        [Description("04")]
        Pensioner = 3,
        [Description("")]
        Unknown = 4
    }
}