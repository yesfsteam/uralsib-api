﻿﻿using System.ComponentModel;

 namespace Uralsib.Api.Models.Enums
{
    public enum UralsibEmployeePosition
    {
        [Description("04")]
        Specialist = 1,
        [Description("03")]
        Manager = 2,
        [Description("01")]
        Head = 3
    }
}