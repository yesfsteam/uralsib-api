﻿using Uralsib.Api.Models.Enums;
using Yes.Infrastructure.Common.Models;
#pragma warning disable 1591

namespace Uralsib.Api.Models
{
    public class UralsibCreditApplicationRequest : JsonModel
    {
        public string Source { get; set; }
        public string AppId { get; set; }
        public string AppDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Sex { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public int AgreeFlag { get; set; }
        public string PlaceOfBirth { get; set; }
        public string IdSeries { get; set; }
        public string IdNum { get; set; }
        public string IssueDate { get; set; }
        public string IssueAuthority { get; set; }
        public string IssueLocation { get; set; }
        public string RegAddrRegion { get; set; }
        public string RegAddrCity { get; set; }
        public string RegAddrStreet { get; set; }
        public string RegAddrHouse { get; set; }
        public string RegAddrFlat { get; set; }
        public int LiveRegAddr { get; set; }
        public string ActAddrRegion { get; set; }
        public string ActAddrCity { get; set; }
        public string ActAddrStreet { get; set; }
        public string ActAddrHouse { get; set; }
        public string ActAddrFlat { get; set; }
        public UralsibEducation? Education { get; set; }
        public int? MonthlyIncome { get; set; }
        public UralsibMaritalStatus? MaritalStatus { get; set; }
        public int? Dependents { get; set; }
        //public decimal? AddIncome { get; set; }
        public string IncomeProof { get; set; }
        //public string SalaryBank { get; set; }
        //public string CreditHistory { get; set; }
        //public decimal? CurrentCreditPay { get; set; }
        public string EmpType { get; set; }
        public string EmpBeginDate { get; set; }
        public string EmpFullName { get; set; }
        public string EmpInn { get; set; }
        public string EmpField { get; set; }
        //public string EmpForm { get; set; }
        public string PositionType { get; set; }
        //public string Position { get; set; }
        public string EmpRegion { get; set; }
        public string EmpCity { get; set; }
        public string EmpStreet { get; set; }
        public string EmpHouse { get; set; }
        public string EmpApartment { get; set; }
        public string WorkPhone { get; set; }
        //public string CreditProposition { get; set; }
        public int CreditSum { get; set; }
        public int CreditPeriod { get; set; }
        public string DoCity { get; set; }
        //public string AddPhone { get; set; }
        //public string AddOwnerPhone { get; set; }
        public int HaveCar { get; set; }
        public int HaveRealState { get; set; }
        //public decimal? PayRent { get; set; }
        //public string PfNum { get; set; }
        public int CommunicationFlag { get; set; }
        public string Product { get; set; }
    }
}