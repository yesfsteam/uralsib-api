﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Uralsib.Api.Models;
using Yes.Infrastructure.Http;

namespace Uralsib.Api.Domain
{
    public interface IUralsibClient
    {
        Task<Response<UralsibCreditApplicationResponse>> CreateCreditApplication(UralsibCreditApplicationRequest request);
    }

    public class UralsibClient : IUralsibClient
    {
        private readonly HttpClient httpClient;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly Uri uralsibUrl; 

        public UralsibClient(HttpClient httpClient, IConfiguration configuration)
        {
            this.httpClient = httpClient;
            jsonSerializerSettings = new JsonSerializerSettings{ContractResolver = new CamelCasePropertyNamesContractResolver()};
            uralsibUrl = configuration.GetValue<Uri>("UralsibClient:Url");
        }

        public async Task<Response<UralsibCreditApplicationResponse>> CreateCreditApplication(UralsibCreditApplicationRequest request)
        {
            return await post<UralsibCreditApplicationResponse>("ShortApp/", request);
        } 
        
        private async Task<Response<T>> post<T>(string uri, object obj)
        {
            var destinationEndpoint = new Uri(uralsibUrl, uri);
            var content = new JsonContent(obj, jsonSerializerSettings);
            content.Headers.Add("X-Destination-Endpoint", destinationEndpoint.ToString());
            
            var request = new HttpRequestMessage(HttpMethod.Post, httpClient.BaseAddress) {Content = content};
            var response = await httpClient.SendAsync(request);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }
    }
}