﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Uralsib.Api.Data;
using Uralsib.Api.Extensions;
using Uralsib.Api.Models;
using Uralsib.Api.Models.Configuration;
using Uralsib.Api.Models.Enums;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;
#pragma warning disable 1591

namespace Uralsib.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IUralsibClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, IUralsibClient client, ApplicationConfiguration configuration,
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.CreditAmount < configuration.MinCreditAmount)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, $"Минимальная сумма кредита: {configuration.MinCreditAmount}");
                    logger.LogInformation($"CheckCreditApplication. Skip. Credit amount too small. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                if (@event.ProfileType == ProfileType.Short || @event.ProfileType == ProfileType.Medium)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой и средней анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                var appId = createAppId(beginTime);
                var request = createRequest(@event, beginTime, appId, 1);
                var response = await client.CreateCreditApplication(request);

                await saveCreditApplicationCheckRequest(@event.CreditApplicationId, beginTime, request.AppId, response);
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.ApproveFlag == 1)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId, response.Content.Message);
                        logger.LogInformation($"CheckCreditApplication. Check request accepted. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();
                    }
                    else
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Declined, @event.CreditApplicationId, response.Content.ErrorMessage ?? response.Content.Message);
                        logger.LogInformation($"CheckCreditApplication. Check request declined. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();
                    }
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)//todo
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)//todo
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} CreditApplicationId: {@event.CreditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing check request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing check request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }
/*
        public async Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.CreditAmount < configuration.MinCreditAmount)
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Skip. Credit amount too small. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, $"Минимальная сумма кредита: {configuration.MinCreditAmount}");
                }
                var appId = createAppId(beginTime);
                var request = createRequest(model, beginTime, appId);
                var response = await client.CreateCreditApplication(request);

                await saveCreditApplicationCheckRequest(creditApplicationId, beginTime, request.AppId, response);
                    
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.ApproveFlag == 1)
                    {
                        logger.LogInformation($"ResendCreditApplicationRequest. Check request accepted. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Approved);
                    }

                    logger.LogInformation($"ResendCreditApplicationRequest. Check request declined. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Declined, response.Content.ErrorMessage ?? response.Content.Message);
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)//todo
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)//todo
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorCheck, response.ErrorMessage);
                }

                logger.LogWarning($"ResendCreditApplicationRequest. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ResendCreditApplicationRequest. Error while processing check request. Duration: {beginTime.GetDuration()} Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }
        */
        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.ProfileType == ProfileType.Short || model.ProfileType == ProfileType.Medium)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой и средней анкеты");
                }

                var appId = createAppId(beginTime);
                var request = createRequest(model, beginTime, appId, 1);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationConfirmRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    UralsibAppId = request.AppId,
                    UralsibApproveFlag = response.Content?.ApproveFlag,
                    UralsibMessage = response.Content?.Message,
                    UralsibErrorCode = response.Content?.ErrorCode,
                    UralsibErrorMessage = response.Content?.ErrorMessage
                });

                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.ApproveFlag == 1)
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirmation request confirmed. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed);
                    }

                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request rejected. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Rejected, response.Content.ErrorMessage ?? response.Content.Message);
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request validation fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Confirmation request authorization fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Confirmation request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing confirmation request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private UralsibCreditApplicationRequest createRequest(CreditApplicationRequest request, DateTime date, string appId, int communicationFlag = 0)
        {
            var isResidenceAddressEmpty = request.IsResidenceAddressEmpty();
            var registrationAddressHouse = $"{request.RegistrationAddressHouse} {request.RegistrationAddressBlock} {request.RegistrationAddressBuilding}";
            return new UralsibCreditApplicationRequest
            {
                Source = configuration.Source,
                AppId = appId,
                AppDate = date.ToUralsibDateFormat(),
                LastName = request.LastName,
                FirstName = request.FirstName,
                MiddleName = request.MiddleName,
                Sex = request.Gender.HasValue ? ((int) (request.Gender == Gender.Male ? Sex.Male : Sex.Female)).ToString() : null,
                DateOfBirth = request.DateOfBirth.ToUralsibDateFormat(),
                Email = request.Email,
                MobilePhone = request.PhoneNumber,
                AgreeFlag = request.PersonalDataProcessApproved ? 1 : 0,
                PlaceOfBirth = request.PlaceOfBirth,
                IdSeries = request.PassportSeries,
                IdNum = request.PassportNumber,
                IssueDate = request.PassportIssueDate.ToUralsibDateFormat(),
                IssueAuthority = request.PassportIssuer,
                IssueLocation = request.PassportDepartmentCode,
                RegAddrRegion = getRegionCode(request.RegistrationAddressRegionKladrCode),
                RegAddrCity = request.RegistrationAddressCity,
                RegAddrStreet = request.RegistrationAddressStreet,
                RegAddrHouse = registrationAddressHouse,
                RegAddrFlat = request.RegistrationAddressApartment,
                LiveRegAddr = getAddressEquals(request, isResidenceAddressEmpty),
                ActAddrRegion = isResidenceAddressEmpty ? getRegionCode(request.RegistrationAddressRegionKladrCode) : getRegionCode(request.ResidenceAddressRegion),
                ActAddrCity = isResidenceAddressEmpty ? request.RegistrationAddressCity : request.ResidenceAddressCity,
                ActAddrStreet = isResidenceAddressEmpty ? request.RegistrationAddressStreet : request.ResidenceAddressStreet,
                ActAddrHouse = isResidenceAddressEmpty ? 
                    registrationAddressHouse : 
                    $"{request.ResidenceAddressHouse} {request.ResidenceAddressBlock} {request.ResidenceAddressBuilding}",
                ActAddrFlat = isResidenceAddressEmpty ? request.RegistrationAddressApartment : request.ResidenceAddressApartment,
                Education = request.Education.ToUralsibEducation(),
                MonthlyIncome = request.MonthlyIncome.HasValue ? (int?)request.MonthlyIncome : null,
                MaritalStatus = request.MaritalStatus.ToUralsibMaritalStatus(),
                Dependents = request.DependentsCount,
                IncomeProof = request.ConfirmationDocument.GetDescription(),
                EmpType = request.Activity.ToUralsibActivity(request.DateOfBirth),
                EmpBeginDate = request.EmployeeStartDate.ToUralsibDateFormat(),
                EmpFullName = request.EmployerName,
                EmpInn = request.Tin,
                EmpField = request.EmployerIndustry.ToUralsibEmployerIndustry(), 
                PositionType = request.EmployeePosition.ToUralsibEmployeePosition(),
                EmpRegion = request.EmployerAddressRegion,
                EmpCity = request.EmployerAddressCity,
                EmpStreet = request.EmployerAddressStreet,
                EmpHouse = $"{request.EmployerAddressHouse} {request.EmployerAddressBlock} {request.EmployerAddressBuilding}",
                EmpApartment = request.EmployerAddressApartment,
                WorkPhone = request.EmployerPhoneNumber,
                CreditSum = request.CreditAmount ?? 0,
                CreditPeriod = request.CreditPeriod / 30 ?? 0,
                DoCity = string.IsNullOrEmpty(request.ResidenceAddressCity) ? request.RegistrationAddressCity : request.ResidenceAddressCity,
                HaveCar = request.HasCar ? 1 : 0,
                HaveRealState = request.HasFlat || request.HasHouse || request.HasArea ? 1 : 0,
                CommunicationFlag = communicationFlag, //todo убрать параметр communicationFlag, если всегда 1
                Product = configuration.Product
            };
        }

        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }
        
        private int getAddressEquals(CreditApplicationRequest request, bool isResidenceAddressEmpty)
        {
            return request.RegistrationAddressRegion == request.ResidenceAddressRegion &&
                   request.RegistrationAddressCity == request.ResidenceAddressCity &&
                   request.RegistrationAddressStreet == request.ResidenceAddressStreet &&
                   request.RegistrationAddressHouse == request.ResidenceAddressHouse &&
                   request.RegistrationAddressBlock == request.ResidenceAddressBlock &&
                   request.RegistrationAddressBuilding == request.ResidenceAddressBuilding &&
                   request.RegistrationAddressApartment == request.ResidenceAddressApartment
                ? 1
                : isResidenceAddressEmpty
                    ? 1
                    : 0;
        }
        
        private string createAppId(DateTime date)
        {
            return $"{configuration.Prefix}{date.ToUralsibAppIdFormat()}{Guid.NewGuid()}";
        }
        
        private string getRegionCode(string regionCodeKladr)
        {
            if (string.IsNullOrWhiteSpace(regionCodeKladr) || regionCodeKladr.Length < 2)
                return "00";
            return regionCodeKladr.Substring(0, 2);
        }

        private async Task saveCreditApplicationCheckRequest(Guid creditApplicationId, DateTime date, string appId, Response<UralsibCreditApplicationResponse> response)
        {
            await repository.SaveCreditApplicationCheckRequest(new CreditApplicationRequestModel
            {
                CreditApplicationId = creditApplicationId,
                CreatedDate = date,
                HttpStatusCode = response.StatusCode,
                ErrorMessage = response.ErrorMessage,
                UralsibAppId = appId,
                UralsibApproveFlag = response.Content?.ApproveFlag,
                UralsibMessage = response.Content?.Message,
                UralsibErrorCode = response.Content?.ErrorCode,
                UralsibErrorMessage = response.Content?.ErrorMessage
            });
        }
    }
}