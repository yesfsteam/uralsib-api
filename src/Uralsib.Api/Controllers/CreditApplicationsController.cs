﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uralsib.Api.Domain;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace Uralsib.Api.Controllers
{
    [Route("api/v1/credit-applications")]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationsController(ICreditApplicationManager manager)
        {
            this.manager = manager;
        }
        /*
        /// <summary>
        /// Отправляет заново запрос на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/retry")]
        [ProducesResponseType(typeof(CreditApplicationResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> ResendCreditApplicationRequest([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await manager.ResendCreditApplicationRequest(creditApplicationId, model);
            return MakeResponse(response);
        }
*/
        /// <summary>
        /// Подтверждает заявку на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/confirmation")]
        [ProducesResponseType(typeof(CreditApplicationResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> ConfirmCreditApplication([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await manager.ConfirmCreditApplication(creditApplicationId, model);
            return MakeResponse(response);
        }
    }
}