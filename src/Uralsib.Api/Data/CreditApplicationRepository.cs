﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Uralsib.Api.Models;

namespace Uralsib.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationCheckRequest(CreditApplicationRequestModel model);
        Task<CreditApplicationRequestModel> GetLastCheckRequest(Guid creditApplicationId);
        Task SaveCreditApplicationConfirmRequest(CreditApplicationRequestModel model);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationCheckRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.UralsibAppId,
                model.UralsibApproveFlag,
                model.UralsibMessage,
                model.UralsibErrorCode,
                model.UralsibErrorMessage,
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_check_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    uralsib_app_id,
                    uralsib_approve_flag,
                    uralsib_message,
                    uralsib_error_code,
                    uralsib_error_message
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :uralsibAppId,
                    :uralsibApproveFlag,
                    :uralsibMessage,
                    :uralsibErrorCode,
                    :uralsibErrorMessage
                );", queryParams);
        }
        
        public async Task<CreditApplicationRequestModel> GetLastCheckRequest(Guid creditApplicationId)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationRequestModel>($@"
                SELECT
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    uralsib_app_id,
                    uralsib_approve_flag,
                    uralsib_message,
                    uralsib_error_code,
                    uralsib_error_message
                FROM credit_application_check_request
                WHERE
                    credit_application_id = :creditApplicationId::uuid
                ORDER BY
                    created_date DESC;", new {creditApplicationId});
        }

        public async Task SaveCreditApplicationConfirmRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.UralsibAppId,
                model.UralsibApproveFlag,
                model.UralsibMessage,
                model.UralsibErrorCode,
                model.UralsibErrorMessage,
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_confirm_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    uralsib_app_id,
                    uralsib_approve_flag,
                    uralsib_message,
                    uralsib_error_code,
                    uralsib_error_message
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :uralsibAppId,
                    :uralsibApproveFlag,
                    :uralsibMessage,
                    :uralsibErrorCode,
                    :uralsibErrorMessage
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}