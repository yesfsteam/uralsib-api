﻿using System.Threading.Tasks;
using SD.Cqrs;
using SD.Messaging.Models;
using Uralsib.Api.Domain;
using Yes.CreditApplication.Api.Contracts.Events;

namespace Uralsib.Api.Messaging
{
    public class CreditApplicationProjection : Projection
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationProjection(ICreditApplicationManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(CreditApplicationCreatedEvent @event)
            => await manager.CheckCreditApplication(@event);
    }
}